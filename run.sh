
#!/bin/bash

#-----------------------------------------------------------------------------#
### Author

#@author: (L.Cook) ljc237@cornell.edu
#@maintainer: (J.L.S.) jdl232@cornell.edu

#-----------------------------------------------------------------------------#
### Set bash

set -e # Abort script at first error, when a command exits with non-zero status
set -u # Attempt to use undefined variable outputs error message, and forces
# an exit

#-----------------------------------------------------------------------------#

# Default Values
HOST="localhost"
PORT="5432"
CROP="dev"
SCRIPT_ROOT="/data/gobii_bundle/"

PARAMS=""
while (( "$#" )); do
  case "$1" in

    -a|--aspect)
      if [ -n "$2" ]; then
        ASPECT=$2
        shift 2
      else
        echo "Error: Argument for $1 is missing" >&2
        exit 1
      fi
      ;;

    -d|--data)
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        DATA=$2
        shift 2
      else
        echo "Error: Argument for $1 is missing" >&2
        exit 1
      fi
      ;;

    -t|--type)
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        DATASET_TYPE=$2
        shift 2
      else
        echo "Error: Argument for $1 is missing" >&2
        exit 1
      fi
      ;;

    -c|--crop)
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        CROP=$2
        shift 2
      else
        echo "Error: Argument for $1 is missing" >&2
        exit 1
      fi
      ;;

    -s|--scriptRoot)
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        SCRIPT_ROOT=$2
        shift 2
      else
        echo "Error: Argument for $1 is missing" >&2
        exit 1
      fi
      ;;



    -H|--hdf5)
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        HDF5=$2
        shift 2
      else
        echo "Error: Argument for $1 is missing" >&2
        exit 1
      fi
      ;;

    -u|--user)
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        USER=$2
        shift 2
      else
        echo "Error: Argument for $1 is missing" >&2
        exit 1
      fi
      ;;

    -p|--password)
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        PASSWORD=$2
        shift 2
      else
        read -s -p "Enter Password: " PASSWORD
        echo ""
        shift 1
      fi
      ;;

    -h|--host)
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        HOST=$2
        shift 2
      else
        echo "Error: Argument for $1 is missing" >&2
        exit 1
      fi
      ;;

    -P|--port)
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        PORT=$2
        shift 2
      else
        echo "Error: Argument for $1 is missing" >&2
        exit 1
      fi
      ;;

    -D|--database)
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        DATABASE=$2
        shift 2
      else
        echo "Error: Argument for $1 is missing" >&2
        exit 1
      fi
      ;;


    -*|--*=) # unsupported flags
      echo "Error: Unsupported flag $1" >&2
      exit 1
      ;;

    *) # preserve positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac done
eval set -- "$PARAMS"

case $DATASET_TYPE in
    NUCLEOTIDE_4_LETTER)
      SIZE="4"
      ;;
    NUCLEOTIDE_2_LETTER | IUPAC | VCF)
      SIZE="2"
      ;;
    SSR_ALLELE_SIZE)
      SIZE="8"
      ;;
    CO_DOMINANT_NON_NUCLEOTIDE | DOMINANT_NON_NUCLEOTIDE)
      SIZE="1"
      ;;
esac


BASEDIR=/data/gobii_bundle/crops/$CROP/loader/digest
TMPDIR=$(mktemp -d --tmpdir=$BASEDIR)
INTERMEDIATE=$TMPDIR/intermediate
OUTPUT=$TMPDIR/output
GOBII_IFL=$SCRIPT_ROOT"loaders/postgres/gobii_ifl/gobii_ifl.py"

mkdir $INTERMEDIATE
mkdir $OUTPUT

chmod 777 $TMPDIR # forgive me lord, for I have sinned


echo INTERMEDIATE FILES: $INTERMEDIATE
echo OUTPUT FILES: $OUTPUT
java -jar Masticator.jar -a $ASPECT -d $DATA -o $INTERMEDIATE

PY_DB_URL="postgresql://$USER:$PASSWORD@$HOST:$PORT/$DATABASE"

[ -f $INTERMEDIATE/digest.matrix ] && { MATRIX_FILE=$TMPDIR/digest.matrix; mv $INTERMEDIATE/digest.matrix $MATRIX_FILE; }

TABLE_LIST=`cat $ASPECT | python -mjson.tool | grep '^        "' | grep -oE '[A-Za-z]+'`

#TODO: exclude 'matrix' from TABLE_LIST
echo $TABLE_LIST |  xargs -I % $GOBII_IFL -v -c $PY_DB_URL -i $INTERMEDIATE/digest.% -o $OUTPUT

#find $INTERMEDIATE/* -exec $GOBII_IFL -v -c $PY_DB_URL -i {} -o $OUTPUT \;

LOADHDF5PATH=/data/gobii_bundle/loader/something/loadHDF5
if [ -z ${MATRIX_FILE+""} ]; then echo ""; else 
  $LOADHDF5PATH $MATRIX_FILE $SIZE $MATRIX_OUT
  echo "var is set to '$var'"
fi



