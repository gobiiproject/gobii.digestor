package org.gobii.digester;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.gobii.aspects.AspectParser;
import org.gobii.aspects.ElementAspect;
import org.gobii.aspects.FileAspect;
import org.gobii.digester.deglutator.Loader;
import org.gobii.digester.deglutator.loader.Hdf5MatrixLoader;
import org.gobii.digester.deglutator.loader.PostgresMetadataLoader;
import org.gobii.digester.masticator.Masticator;
import org.gobii.digester.util.Util;

public class Digester {

	private FileAspect aspect;

	private String dbUrl;
	private String hdf5Location;
	private int matrixSize;

	public Digester(String dbUrl, String hdf5Location, int matrixSize, FileAspect aspect) {
		this.dbUrl = dbUrl;
		this.aspect = aspect;
		this.matrixSize = matrixSize;
		this.hdf5Location = hdf5Location;
	}

	public void run(File file) throws Exception {

		Map<String, List<String>> tableHeaders = new HashMap<>();

		aspect.getAspects().values().forEach(
				tableAspect -> tableHeaders.put(tableAspect.getTable(), new ArrayList<>(tableAspect.getAspects().keySet())
		));


		Map<String, Loader> loaders = new HashMap<>();
		for (String table : aspect.getAspects().keySet()) {
			loaders.put(table, new PostgresMetadataLoader(dbUrl, table, tableHeaders.get(table)));
		}

		Loader matrixLoader = new Hdf5MatrixLoader(new File(hdf5Location), matrixSize);
		loaders.put("matrix", matrixLoader);

		Masticator masticator = new Masticator(aspect, file);

		for (String table : aspect.getAspects().keySet()) {

			PipedOutputStream masticatorOut = new PipedOutputStream();
			PipedInputStream deglutatorIn = new PipedInputStream(masticatorOut);

			new Thread(() -> {
				Writer writer = new PrintWriter(masticatorOut);
				try {
					if (! "matrix".equals(table)) {
						writer.write(tableHeader(table) + "\n");
						writer.flush();
					}

					masticator.run(table, writer);
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						masticatorOut.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}).start();

			final Loader deglutator = loaders.get(table);

			new Thread(() ->{
				try {
					BufferedReader reader = new BufferedReader(new InputStreamReader(deglutatorIn));


					String read;
					while ((read = reader.readLine()) != null) {
						deglutator.load(read);
					}

					deglutator.commit();

				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						deglutatorIn.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}).start();
		}
	}

	private String tableHeader(String table) {
		return aspect
				.getAspects()
				.get(table)
				.getAspects()
				.values()
				.stream()
				.map(ElementAspect::getName)
				.collect(Collectors.joining("\t"));
	}

	public static void main(String[] args) throws Exception {

		String aspectPath = "/Users/ljc237-admin/gobii/gobii.digester/src/main/resources/matrix_load.json";
		String data = "/Users/ljc237-admin/gobii/gobii.digester/src/main/resources/matrix.tsv";
		String dbUrl = "jdbc:postgresql://cbsugobiixvm16.biohpc.cornell.edu:5433/gobii_dev?user=appuser&password=g0b11isw3s0m3&";

//		String aspectPath = args[0];
//		String data = args[1];
//		String dbUrl = args[2];
//		String hdf5Location = args[3];
//		int matrixSize = Integer.parseInt(args[4]);
		FileAspect aspect = AspectParser.parse(Util.slurp(aspectPath));

		new Digester(dbUrl, "/temp/asdf", 0, aspect).run(new File(data));

	}

}
